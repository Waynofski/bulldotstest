<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Translation;
use App\Photo;

class News extends Model
{
    //

    protected $fillable = ['title','body','user_id','intro'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function translation()
    {
        return $this->belongsTo('App\Translation');
    }
    public function photo(){
        return $this->belongsTo('App\Photo');
    }
}
