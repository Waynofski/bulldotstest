<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;

class ContactController extends Controller
{
    public function index(){
        //
        return view('contact');
    }
    public function store(Request $request){
        $this->validate($request,[
            'firstName'=>'required',
            'lastName'=>'required',
            'email'=>'required',
            'message'=>'required',
        ]);
        Mail::send('contact.mail',[
            'msg'=>$request->message
        ],function ($mail) use($request){
            $mail->from($request->email, $request->firstName);

            $mail->to('info@client.be')->subject('contact');
        });
        return redirect()->back()->with('message','thank you for contacting us');
    }
}
