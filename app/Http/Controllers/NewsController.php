<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\News;
class NewsController extends Controller
{
    //
    public function index()
    {

        $news = News::all();
        $users = User::all();

        return view('news',compact('news','users'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        return view('news_detail', ['new' => News::findOrFail($id)]);
    }
}
