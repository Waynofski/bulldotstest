<?php

namespace App\Http\Controllers;
use App\Translation;
use App\Http\Traits\Translatable;
use App\News;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class   CmsNewsController extends Controller
{
    use translatable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $news = News::all();
        return view('cms/news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $user =User::pluck('name','id')->all();
        return view('cms.news.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $input = $request->all();


        if($file = $request->file('photo_id')){
            $name = time() . $file->getClientOriginalName();
            $file->move('img',$name);
            $photo = Photo::create(['file'=>$name]);
            $input['photo_id'] = $photo->id;
        }
        News::create($input);

        return redirect('cms/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $new = News::findOrFail($id);
        $user =User::pluck('name','id')->all();
        return view('cms/news/edit',compact('new','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        //
        $input = $request->all();
        $new = News::findOrFail($id);
        if($file = $request->file('photo_id')){
            $name = time().$file->getClientOriginalName();
            $file->move('images',$name);
            $photo= Photo::create(['file' =>$name]);
            $input['photo_id']= $photo->id;
        }


        $new->update($input);
        return redirect('/cms/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $news = News::findOrFail($id);
        unlink(public_path() . $news->photo->file);
        $news->delete();
        return redirect('cms.news.index');
    }
}
