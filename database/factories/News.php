<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\News;
use Faker\Generator as Faker;

$factory->define(News::class, function (Faker $faker) {
    return [

   'user_id'=>$faker->numberBetween($min = 1,$max = 5),
    'photo_id'=>$faker->numberBetween($min = 1,$max = 5),
   'title'=>$faker->sentence($nbWords = 6,$variableNbWords = true),
    'intro'=> $faker->paragraph,
    'body'=>$faker->paragraph,
    ];
});
