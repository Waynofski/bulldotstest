<x-layout>
    <div class="container mt-5">
        <div class="row">
            @if($news)
            @foreach ($news as $new)
                <div class="col-md-4">
                    <a class="card mb-4 shadow-sm text-decoration-none" href="#">
                        <svg class="bd-placeholder-img card-img-top" width="100%" height="225"
                             xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false"
                             role="img" aria-label="Placeholder: Thumbnail"><title>{{$new->title}}</title>
                            <rect width="100%" height="100%" fill="#55595c"></rect>
                            <text x="50%" y="50%" fill="#eceeef" dy=".3em">Thumbnail</text>
                        </svg>{route('profile', ['id' => 1]
                        <div class="card-body text-dark">
                            <h3><a href="{{route('news_detail', ['id' => $new->id])}}">{{$new->title}}</a></h3>
                            <p class="card-text">{{$new->intro}}</p>
                            <div class="text-right">
                                <small class="text-muted">By {{$new->user->name}}</small>
                            </div>
                        </div>
                    </a>
                </div>
            @endforeach
                @endif
        </div>
    </div>
</x-layout>
