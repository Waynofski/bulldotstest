<x-cms>
    <div class="container-fluid">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">News</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Title</th>
                            <th>Intro</th>
                            <th>Author</th>
                            <th>Created At</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Title</th>
                            <th>Intro</th>
                            <th>Author</th>
                            <th>Created At</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        @if($news)
                            @foreach($news as $new)
                                <tr>

                                    <td>{{$new->title}}</td>
                                    <td>{{$new->body}}</td>
                                    <td><a href="{{route('news.edit',$new->id)}}">{{$new->user->name}}</a></td>
                                    <td>{{$new->created_at}}</td>
                                </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</x-cms>
