<x-cms>
    <div class="container">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Editing news item</h6>
            </div>
            <div class="card-body">
                {!! Form::model($new,['method'=>'PATCH','action'=>['CmsNewsController@update',$new->id],'files'=>true]) !!}

                <div class="form-group">
                    {!! Form::label('user_id','Author:') !!}
                    {!! Form::select('user_id',[''=>'Choose Author']+$user,null, ['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('title','Title:') !!}
                    {!! Form::text('title',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('intro','Intro:') !!}
                    {!! Form::textarea('intro',null,['class'=>'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('body','Html:') !!}
                    {!! Form::textarea('body',null,['class'=>'form-control']) !!}
                </div>
                {{--                <div class="form-group">--}}
                {{--                    {!! Form::label('photo_id','Photo:') !!}--}}
                {{--                    {!! Form::file('photo_id',null,['class'=>'form-control']) !!}--}}

                {{--                </div>--}}


                <div class="form-group">
                    {!! Form::submit('Create news', ['class'=>'btn btn-success btn-block']) !!}
                </div>
                {!! Form::close() !!}
                {!! Form::open(['method'=>'DELETE','action'=>['CmsNewsController@destroy', $new->id]]) !!}
                <div class="form-group">
                    {!! Form::submit('Delete News',['class'=>'btn btn-danger btn-block ']) !!}
                </div>
                {!! Form::close() !!}
        </div>

    </div>
</x-cms>
