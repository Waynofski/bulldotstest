<x-layout>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <h4 class="mb-3">Contact us</h4>
                <div class="row">
                <div class="col-md-6 mx-auto">
                    @if(Session::has('flash-message'))
                        <div class="alert alert-success">
                            {{Session::get('message')}}
                        </div>
                        @endif
                <form method="post" action="{{route('contact.store')}}">
                    {{csrf_field()}}

                    <div class="form-group">
                            <label for="firstName">First name*</label>
                            <input type="text" class="form-control " name="firstName" placeholder=""  required="">
                            <div class="invalid-feedback">
                                Valid first name is required.
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="lastName">Last name*</label>
                            <input type="text" class="form-control" name="lastName" placeholder=""  required="">
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>


                    <div class="form-group">
                        <label for="email">Email*</label>
                        <input type="email" class="form-control" name="email" placeholder="" required="">
                        <div class="invalid-feedback">
                            Please enter a valid email address.
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="message">Message*</label>
                        <textarea name="message" cols="30" rows="10" class="form-control"></textarea>
                        <div class="invalid-feedback">
                            Valid messages is required.

                        </div>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Send</button>
                </form>
    </div>
                </div>

            </div>
        </div>
    </div>
</x-layout>
