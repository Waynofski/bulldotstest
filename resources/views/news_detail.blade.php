<x-layout>
    <div class="container mt-5">
        @if($new)
        <h2>{{$new->title}}</h2>
        <p>{{$new->body}}</p>
            @endif
    </div>
</x-layout>
